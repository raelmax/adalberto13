# -*- coding: utf-8 -*-
from django.db import models

class Participar(models.Model):
    nome = models.CharField(max_length=255)
    email = models.EmailField("E-mail")
    endereco = models.CharField(max_length=255)
    bairro = models.CharField(max_length=15)
    telefone = models.CharField(max_length=15)
    voluntario = models.BooleanField()
    doacao = models.BooleanField()
    santinho = models.BooleanField()
    bandeira = models.BooleanField()
    adesivo = models.BooleanField()
    receber_email = models.BooleanField()

    def __unicode__(self):
        return '%s - %s' % (self.nome, self.email)