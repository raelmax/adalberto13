from django.conf.urls.defaults import *
from adalberto13.apps.participar.views import ParticiparView

urlpatterns = patterns('',
    url(r'^$', ParticiparView.as_view(), name='participar'),
    (r'^sucesso/$', 'django.views.generic.simple.direct_to_template', {'template': 'sucesso.html'} ),
)