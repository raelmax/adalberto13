# -*- coding: utf-8 -*-
from django.views.generic.edit import CreateView
from adalberto13.apps.participar.models import Participar


class ParticiparView(CreateView):
    model = Participar
    template_name = 'base.html'
    success_url = '/participar/sucesso'